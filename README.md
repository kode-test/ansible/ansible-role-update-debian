Role Name
=========

Updating debian and CentOS systems


Role Variables
--------------
default vars 
yum_update_name: "*" 
yum_update_state:  latest
yum_update_exclude: ""



Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

 ```
- hosts: apt
   become: yes
   roles:
     - updateOS

- hosts: yum
  become: yes
  roles:
    - updateOS
```



License
-------

BSD


